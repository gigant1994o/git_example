package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {

    public static void main(String[] args) {
		        System.out.println("HAJOX");
		        System.out.println("barev");
		        System.out.println("barev");
		        System.out.println("barev");
		        System.out.println("barev");

        Musician m = new Musician() {
            @Override
            public void playMusic() {
                System.out.println("playing piano");
            }
        };
        m.playMusic();

        Musician mm = () -> {
            System.out.println("barev");
            System.out.println("barev");
            System.out.println("barev");
        };
        mm.playMusic();

        Mathematics math = (b, a) -> {
            return b > a ? b : a;
        };
        System.out.println(math.bigger(3, 7));

        A aa = x -> System.out.println(++x);
        aa.f(34);

        List<Integer> numbers = new ArrayList<>(Arrays.asList(2, 5, 3, 8));
        numbers.forEach(integer -> System.out.println(++integer));

        List<String> names = new ArrayList<>(
                Arrays.asList("hayk", "aram", "ani", "marine")
        );
//        List<String> namesUpper = new ArrayList<>();
//        for (String x: names){
//            namesUpper.add(x.toUpperCase());
//        }
//        System.out.println(namesUpper);
//        List<String> namesUpper = names.stream()
//                .map(x -> "barev " + x.toUpperCase())
//                .collect(Collectors.toList());
//        System.out.println(namesUpper);
        List<String> namesUpper = names.stream()
                .filter(a -> a.contains("r") || a.contains("R"))
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(namesUpper);


        System.out.println(numbers);
        Integer integer = numbers
                .stream()
                .filter(x -> x >= 1 && x <= 6)
                .reduce(Integer::max).get();
        System.out.println(integer);
    }

}
    